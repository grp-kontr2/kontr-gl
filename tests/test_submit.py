import uuid
from pathlib import Path

import git
import pytest

from kontr_gl.submit import Submitter


@pytest.fixture()
def gl_url():
    return 'https://localhost'

@pytest.fixture()
def gl_user(gl_url):
    uname = 'admin'
    return {
        "id": 26,
        "name": "Test user",
        "username": uname,
        "state": "active",
        "avatar_url": f"{gl_url}/uploads/-/system/user/avatar/26/avatar.png",
        "web_url": f"{gl_url}/{uname}",
        "created_at": "2016-04-25T16:46:02.862+02:00",
        "bio": "",
        "location": "",
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": "",
        "last_sign_in_at": "2019-03-29T10:02:25.337+01:00",
        "confirmed_at": "2017-10-12T15:56:15.152+02:00",
        "last_activity_on": "2019-04-02",
        "email": f"{uname}@example.com",
        "theme_id": 2,
        "color_scheme_id": 5,
        "projects_limit": 25,
        "current_sign_in_at": "2019-04-02T17:18:31.223+02:00",
        "identities": [
            {
                "provider": "ldapmain",
                "extern_uid": f"uid={uname},ou=people,dc=example,dc=com"
            }
        ],
        "can_create_group": False,
        "can_create_project": True,
        "two_factor_enabled": False,
        "external": False,
        "private_profile": None,
        "shared_runners_minutes_limit": None
    }


@pytest.fixture()
def gl_project(gl_url, project):
    return {
        "id": 3,
        "description": None,
        "default_branch": "master",
        "visibility": "private",
        "ssh_url_to_repo": f"{project.repo_url_ssh}",
        "http_url_to_repo": f"{project.repo_url_https}",
        "web_url": f"{project.repo_url_https}",
        "readme_url": f"{gl_url}/{project.namespace}/blob/master/README.md",
        "tag_list": [
            "example",
            "disapora project"
        ],
        "owner": {
            "id": 3,
            "name": "Diaspora",
            "created_at": "2013-09-30T13:46:02Z"
        },
        "name": f"{project.name}",
        "name_with_namespace": f"{project.namespace}",
        "path": project.name,
        "path_with_namespace": project.namespace,
        "issues_enabled": True,
        "open_issues_count": 1,
        "merge_requests_enabled": True,
        "jobs_enabled": True,
        "wiki_enabled": True,
        "snippets_enabled": True,
        "resolve_outdated_diff_discussions": False,
        "container_registry_enabled": False,
        "created_at": "2013-09-30T13:46:02Z",
        "last_activity_at": "2013-09-30T13:46:02Z",
        "creator_id": 3,
        "namespace": {
            "id": 3,
            "name": "Diaspora",
            "path": "diaspora",
            "kind": "group",
            "full_path": "diaspora",
            "avatar_url": "http://localhost:3000/uploads/group/avatar/3/foo.jpg",
            "web_url": "http://localhost:3000/groups/diaspora"
        },
        "import_status": "none",
        "import_error": None,
        "permissions": {
            "project_access": {
                "access_level": 10,
                "notification_level": 3
            },
            "group_access": {
                "access_level": 50,
                "notification_level": 3
            }
        },
        "archived": False,
        "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
        "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
        "license": {
            "key": "lgpl-3.0",
            "name": "GNU Lesser General Public License v3.0",
            "nickname": "GNU LGPLv3",
            "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
            "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
        },
        "shared_runners_enabled": True,
        "forks_count": 0,
        "star_count": 0,
        "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
        "ci_default_git_depth": 50,
        "public_jobs": True,
        "shared_with_groups": [
            {
                "group_id": 4,
                "group_name": "Twitter",
                "group_full_path": "twitter",
                "group_access_level": 30
            },
            {
                "group_id": 3,
                "group_name": "Gitlab Org",
                "group_full_path": "gitlab-org",
                "group_access_level": 10
            }
        ],
        "repository_storage": "default",
        "only_allow_merge_if_pipeline_succeeds": False,
        "only_allow_merge_if_all_discussions_are_resolved": False,
        "printing_merge_requests_link_enabled": False,
        "request_access_enabled": False,
        "merge_method": "merge",
        "statistics": {
            "commit_count": 37,
            "storage_size": 1038090,
            "repository_size": 1038090,
            "wiki_size": 0,
            "lfs_objects_size": 0,
            "job_artifacts_size": 0,
            "packages_size": 0
        }
    }


@pytest.fixture()
def gl_merge_request(gl_url, project, submission):
    return {
        "id": 1,
        "iid": 1,
        "project_id": 3,
        "title": f"Submission: {submission.name}",
        "description":  f'Submission: {submission.name}\nAuthor: {submission.config.author}',
        "state": "merged",
        "created_at": "2017-04-29T08:46:00Z",
        "updated_at": "2017-04-29T08:46:00Z",
        "target_branch": "master",
        "source_branch": submission.config.source_branch,
        "upvotes": 0,
        "downvotes": 0,
        "author": {
            "id": 1,
            "name": "Administrator",
            "username": "admin",
            "state": "active",
            "avatar_url": None,
            "web_url": f"{gl_url}/admin"
        },
        "assignee": {
            "id": 1,
            "name": "Administrator",
            "username": "admin",
            "state": "active",
            "avatar_url": None,
            "web_url": f"{gl_url}/admin"
        },
        "source_project_id": 2,
        "target_project_id": 3,
        "labels": [
            "Community contribution",
            "Manage"
        ],
        "work_in_progress": False,
        "milestone": {
            "id": 5,
            "iid": 1,
            "project_id": 3,
            "title": "v2.0",
            "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
            "state": "closed",
            "created_at": "2015-02-02T19:49:26.013Z",
            "updated_at": "2015-02-02T19:49:26.013Z",
            "due_date": "2018-09-22",
            "start_date": "2018-08-08",
            "web_url": f"{gl_url}/my-group/my-project/milestones/1"
        },
        "merge_when_pipeline_succeeds": True,
        "merge_status": "can_be_merged",
        "merge_error": None,
        "sha": "8888888888888888888888888888888888888888",
        "merge_commit_sha": None,
        "user_notes_count": 1,
        "discussion_locked": None,
        "should_remove_source_branch": True,
        "force_remove_source_branch": False,
        "allow_collaboration": False,
        "allow_maintainer_to_push": False,
        "web_url": f"{gl_url}/my-group/my-project/merge_requests/1",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": None,
            "human_total_time_spent": None
        },
        "squash": False,
        "subscribed": False,
        "changes_count": "1",
        "merged_by": {
            "id": 87854,
            "name": "Douwe Maan",
            "username": "DouweM",
            "state": "active",
            "avatar_url": f"{gl_url}/uploads/-/system/user/avatar/87854/avatar.png",
            "web_url": f"{gl_url}/DouweM"
        },
        "merged_at": "2018-09-07T11:16:17.520Z",
        "closed_by": None,
        "closed_at": None,
        "latest_build_started_at": "2018-09-07T07:27:38.472Z",
        "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
        "first_deployed_to_production_at": None,
        "pipeline": {
            "id": 29626725,
            "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
            "ref": "patch-28",
            "status": "success",
            "web_url": f"{gl_url}/my-group/my-project/pipelines/29626725"
        },
        "diff_refs": {
            "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
            "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
            "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
        },
        "diverged_commits_count": 2,
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        }
    }


@pytest.fixture()
def project(kontr_app, st_project):
    return kontr_app.projects.create('simple_project', str(st_project))


@pytest.fixture()
def params(st_project, st_source):
    return {
        'source': {
            'type': 'git',
            'url': str(st_source)
        },
        'project': {
            'name': st_project.name
        },
        'name': 'simple_submission',
        'uuid': uuid.uuid4(),
        'author': 'test_user'
    }


@pytest.fixture()
def submitter(kontr_app, project, params) -> Submitter:
    return Submitter(kontr_app, project, params)


def test_checkout(submitter, project, storage_dir: Path):
    submitter.sub.sources.copy_tree(src_path=project.path)
    submitter.sub.sources.checkout(branch=submitter.branch_name, new=True)
    submitter.checkout()
    subs_dir = storage_dir / 'submissions'
    sub_dir = subs_dir / 'simple_submission'
    assert subs_dir.exists()
    assert sub_dir.exists()
    assert (sub_dir / 'src').exists()
    assert (sub_dir / 'src' / 'solution').exists()
    assert (sub_dir / 'src' / 'solution' / 'main.c').exists()
