from kontr_gl import KontrGlApplication


def test_projects_import(st_project, storage_dir, kontr_app: KontrGlApplication):
    project = kontr_app.projects.create(name='simple', url=str(st_project))

    assert project.path == storage_dir / 'projects' / 'simple'
    assert project.config_path == storage_dir / 'projects' / 'simple' / 'kontr-project.yaml'
