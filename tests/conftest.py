import shutil
from pathlib import Path

import git

from kontr_gl import KontrGlApplication, log_config

import pytest

TEST_RESOURCES = Path(__file__).resolve().parent / 'resources'

log_config.load('d')


@pytest.fixture()
def st_res(tmpdir) -> Path:
    return Path(tmpdir.mkdir('kontr-gl-resources'))


@pytest.fixture()
def st_project(st_res: Path) -> Path:
    fpath = TEST_RESOURCES / 'simple_project'
    ppath = st_res / 'project'
    shutil.copytree(str(fpath), str(ppath))
    repo = git.Repo.init(str(ppath))
    repo.git.add(A=True)
    repo.index.commit("Initial commit - teacher")
    return ppath


@pytest.fixture()
def st_source(st_res: Path) -> Path:
    fpath = TEST_RESOURCES / 'simple_solution'
    ppath = st_res / 'sources'
    shutil.copytree(str(fpath), str(ppath))
    repo = git.Repo.init(str(ppath))
    repo.git.add(A=True)
    repo.index.commit("Initial commit - student")
    return ppath


@pytest.fixture()
def storage_dir(tmpdir) -> Path:
    path = Path(tmpdir.mkdir("kontr-gl-storage"))
    return path


@pytest.fixture()
def kontr_app(storage_dir) -> KontrGlApplication:
    app = KontrGlApplication(env='testing')
    app.cfg.set('storage.path', storage_dir)
    return app
