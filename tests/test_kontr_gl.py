from kontr_gl import __version__, KontrGlApplication

from pathlib import Path


def test_version():
    assert __version__ == '0.1.0'


def test_kontr_app_validate_paths(kontr_app: KontrGlApplication, storage_dir):
    storage_path = Path(storage_dir)
    assert kontr_app
    assert kontr_app.storage_path == storage_path
    assert kontr_app.projects.get('foo').path == (storage_path / 'projects' / 'foo')
    assert kontr_app.submissions.get('bar').path == (storage_path / 'submissions' / 'bar')
    assert kontr_app.submissions.get('bar').sources.path == (storage_path / 'submissions' / 'bar' / 'src')
    assert kontr_app.submissions.get('bar').results.path == (storage_path / 'submissions' / 'bar' / 'res')
    assert kontr_app.submissions.get('bar').workspaces.path == (storage_path / 'submissions' / 'bar' / 'wrk')

