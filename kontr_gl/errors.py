class KontrGlError(Exception):
    pass


class InvalidPath(KontrGlError):
    pass


class NotFoundError(KontrGlError):
    pass


class DirectoryAlreadyExists(KontrGlError):
    pass


class ProjectAlreadyExists(DirectoryAlreadyExists):
    def __init__(self, name: str, *args):
        super().__init__(f"Project '{name}' already exists.", *args)


class ProjectNotExists(NotFoundError):
    def __init__(self, name: str, *args):
        super(ProjectNotExists, self).__init__(f"Project '{name}' not exist.", *args)


class SubmissionNotExists(NotFoundError):
    def __init__(self, name: str, *args):
        super().__init__(f"Submission '{name}' not exist.", *args)


class SubmissionAlreadyExists(DirectoryAlreadyExists):
    def __init__(self, name: str, *args):
        super().__init__(f"Submission '{name}' already exists.", *args)