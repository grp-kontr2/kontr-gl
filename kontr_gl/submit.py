import logging
import shutil
from pathlib import Path
from typing import Any, MutableMapping, Union, Dict, TYPE_CHECKING
from kontr_gl import st_utils, entities, utils, gl_client

log = logging.getLogger(__name__)

if TYPE_CHECKING:
    from kontr_gl import KontrGlApplication


class Submitter:
    def __init__(self, app: 'KontrGlApplication', project: entities.Project, params: MutableMapping[str, Any]):
        self._app = app
        self._project = project
        self._params = params
        self._sub = self._app.submissions.create(params)
        self.sub.config.set('source.subdir', params.get('subdir', ''))

    @property
    def project(self) -> entities.Project:
        return self._project

    @property
    def sub(self) -> entities.Submission:
        return self._sub

    @property
    def gl_project(self) -> gl_client.KontrGitlabProject:
        return self._app.gitlab.for_entity(self.project)

    @property
    def branch_name(self) -> str:
        return f'sub-{self.sub.name}'

    def submit(self):
        log.info(f"[SUBMIT] Submission: {self.sub.name} for {self.project.name}")

        # Prepare workspace (copy files from the project)
        self.project.pull()
        self.sub.sources.copy_tree(src_path=self.project.path)
        self.sub.sources.checkout(branch=self.branch_name, new=True)

        # Checkout student's sources
        check_result = self.checkout(checkout_type=self.sub.config.source_type)
        result = dict(checkout=check_result)

        if check_result is None:
            msg = f"Unable to checkout, unknown checkout type: {self.sub.config.source_type}"
            self._fail(result, msg)
            return None

        self._save(result)

        result['push'] = self.push()

        result['merge_request'] = self.merge_request()

        return result

    def checkout(self, checkout_type='git'):
        log.info(f"[CHECKOUT] Checking out the the submission: {self.sub.name},"
                 f" type={checkout_type}")
        if checkout_type == 'git':
            return self._git_checkout()

        elif checkout_type == 'file':
            return self._file_checkout()

        elif checkout_type == 'directory':
            return self._directory_checkout()

        elif checkout_type == 'zip':
            return self._zip_checkout()

        return None

    def _zip_checkout(self, from_path=None):
        from_path = Path(from_path if from_path is not None else self.sub.config.source_url)
        workspace = self.workspace('zip')

        st_utils.unzipdir(from_path, dest=workspace.path)
        self._filter(workspace.path)

        result = dict(
            hash=st_utils.calculate_hash(from_path),
            zip_size=st_utils.get_size(from_path),
            size=st_utils.get_size(workspace.path)
        )

        # Tear down
        workspace.delete()
        return result

    def _directory_checkout(self, from_path: Path = None):
        from_path = Path(from_path if from_path is not None else self.sub.config.source_url)
        self._filter(from_path)
        return dict(
            hash=st_utils.calculate_hash(from_path),
            size=st_utils.get_size(from_path)
        )

    def _file_checkout(self, from_path=None):
        from_path = Path(from_path if from_path is not None else self.sub.config.source_url)
        shutil.copy2(from_path, str(self.sub.sources.solution_path))
        return dict(
            hash=st_utils.calculate_hash(from_path),
            size=st_utils.get_size(from_path)
        )

    def _git_checkout(self):
        workspace = self.workspace("git")
        workspace.clone(
            url=self.sub.config.source_url,
            ref=self.sub.config.source_ref,
            branch=self.sub.config.source_branch
        )
        self._filter(workspace.path / self.sub.config.source_subdir)
        result = workspace.git_info()
        workspace.delete()
        return result

    def _filter(self, path: Union[Path, str]):
        st_utils.filter_files(
            from_path=path,
            to_path=self.sub.sources.solution_path,
            allowed=self.project.config.allowed
        )

    def workspace(self, suffix=None) -> utils.GitResource:
        resource = self.sub.workspaces.create(suffix=suffix)
        resource.path.parent.mkdir(parents=True)
        return resource

    def push(self) -> Dict[str, Any]:
        self.sub.sources.push(remote='origin', branch=self.branch_name)
        return dict(branch=self.branch_name, remote='origin')

    def merge_request(self) -> Dict[str, Any]:
        title = f"Submission {self.sub.name}"
        desc = '\n'.join([
            f"Author: {self.sub.config.author}",
            f"Submission name: {self.sub.name}",
            f"Submission branch: {self.branch_name}"
        ])
        mr = self.gl_project.mr_create(
            title=title,
            desc=desc,
            branch_name=self.branch_name
        )
        return mr

    def _save(self, params):
        self.sub.config.dump({'process': params})

    def _fail(self, result, *messages):
        for message in messages:
            result['errors'] = dict(message=message)
            log.error(f"Error: {message}")
        self._save(result)
