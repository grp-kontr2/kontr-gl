import functools
from typing import TYPE_CHECKING, Dict, List

import gitlab
from gitlab.v4 import objects

from kontr_gl import entities, utils

if TYPE_CHECKING:
    from kontr_gl import KontrGlApplication


class KontrGitlabService:
    __slots__ = ('_app', '_client', '_projects_cache')

    def __init__(self, app: 'KontrGlApplication'):
        self._app = app
        self._client = None
        self._projects_cache = {}

    @property
    @functools.lru_cache()
    def url(self) -> str:
        return self._app.cfg.get('gitlab.url')

    @property
    def client(self) -> gitlab.Gitlab:
        if self._client is None:
            token = self._app.cfg.get('gitlab.token')
            self._client = gitlab.Gitlab(
                url=self.url,
                private_token=token
            )
        return self._client

    def for_entity(self, entity: utils.GitResource) -> 'KontrGitlabProject':
        if entity.name not in self._projects_cache:
            self._projects_cache[entity.name] = KontrGitlabProject(self._app, entity)
        return self._projects_cache[entity.name]

    def get_repostory(self, repository_namespace: str) -> objects.Project:
        return self.client.projects.get(repository_namespace)


class KontrGitlabProject:
    __slots__ = ('_ent', '_app', '_gl_project')

    def __init__(self, app: 'KontrGlApplication', ent: utils.GitResource):
        self._ent = ent
        self._app = app
        self._gl_project = None

    @property
    def namespace(self) -> str:
        return self._ent.repo_namespace

    @property
    def _service(self) -> KontrGitlabService:
        return self._app.gitlab

    @property
    def gl_project(self) -> objects.Project:
        if self._gl_project is None:
            project_namespace = self.namespace
            self._gl_project = self._service.client.projects.get(project_namespace)
        return self._gl_project

    def mr_get(self, mr_id: int) -> objects.MergeRequest:
        return self.gl_project.mergerequests.get(mr_id)

    def mr_pipelines(self, mr_id: int) -> List[objects.ProjectPipeline]:
        mr = self.mr_get(mr_id)
        return mr.pipelines()

    def pipeline_get(self, id: int) -> objects.ProjectPipeline:
        return self.gl_project.pipelines.get(id)

    def pipeline_create(self, mr_id: int, branch_name=None) -> objects.ProjectPipeline:
        mr = self.mr_get(mr_id)
        path = f"{mr.manager.path}/{mr.get_id()}/pipelines"
        data = mr.manager.gitlab.http_post(path, )
        return objects.ProjectPipeline(self.gl_project.pipelines, data)

    def mr_create(self, title: str, desc: str, branch_name) -> Dict:
        """
        https://docs.gitlab.com/ee/ci/merge_request_pipelines/
        :return:
        """
        params = dict(
            source_branch=branch_name,
            target_branch='master',
            title=title,
            description=desc
        )

        mr: objects.MergeRequest = self.gl_project.mergerequests.create(params)

        return {
            'params': params,
            'data': {
                'web_url': mr['web_url'],
                'id': mr['id'],
                'iid': mr['iid'],
                'title': mr['title'],
                'description': mr['description'],
                'created_at': mr['created_at'],
                'updated_at': mr['updated_at'],
            }
        }
