import functools
import logging
from pathlib import Path
from typing import Optional, Dict, Union, List

import yaml

from kontr_gl import utils, errors

log = logging.getLogger(__name__)


class KontrProjects(utils.DirectoryMapping):
    @property
    @functools.lru_cache()
    def path(self) -> Path:
        return super().path / self._app.cfg.get('storage.projects_subdir', 'projects')

    def __getitem__(self, name: str) -> 'Project':
        return Project(self.path, name)

    def get(self, name: str) -> 'Project':
        return self[name]

    def create(self, name: str, url: str, ref: str = None, branch: str = None) -> 'Project':
        project = self.get(name)
        if project.exists():
            raise errors.ProjectAlreadyExists(name)
        project.clone(url, ref=ref, branch=branch)
        return project

    def update(self, name: str, remote='origin', branch='master'):
        project = self.get(name)
        if not project.exists():
            raise errors.ProjectNotExists(name)
        project.pull(remote=remote, branch=branch)


class KontrSubmissions(utils.DirectoryMapping):
    @property
    @functools.lru_cache()
    def path(self) -> Path:
        return super().path / self._app.cfg.get('storage.submissions_subdir', 'submissions')

    def __getitem__(self, name: str) -> 'Submission':
        return Submission(self.path, name)

    def get(self, name: str) -> 'Submission':
        return self[name]

    def create(self, params) -> 'Submission':
        sid = params.get('name')
        submission = self.get(sid)
        if submission.exists():
            raise errors.SubmissionAlreadyExists(sid)
        submission.config.init(params)
        return submission

    def update(self, name: str, remote='origin', branch='master'):
        submission = self.get(name)
        if not submission.exists():
            raise errors.SubmissionNotExists(name)
        submission.sources.pull(remote=remote, branch=branch)

    def delete(self, name: str):
        self.get(name).delete()


class Project(utils.GitResource):
    CFG_FILE = 'kontr-project.yaml'

    def __init__(self, base_path: Union[str, Path], name: str):
        super().__init__(base_path, name=name)
        self._config = None

    @property
    def config_path(self) -> Path:
        return self.path / self.__class__.CFG_FILE

    @property
    def config(self) -> Optional['ProjectConfig']:
        if self._config is None:
            self._config = ProjectConfig(self, {})
            self._config.load()
        return self._config

    def info(self) -> Dict:
        return {
            'name': self.name,
            'path': self.str_path,
            'git': self.git_info(),
            'config': self.config.as_dict() if self.exists(self.__class__.CFG_FILE) else None
        }


class ProjectConfig(utils.MappingBase):
    def __init__(self, project: 'utils.GitResource', params=None):
        super().__init__(params=params)
        self._project = project

    def load(self, file: Union[str, Path] = None):
        file = file if file is not None else Project.CFG_FILE
        file = self._project.resolve(file)
        data = self._project.yaml(file) if self._project.exists(file) else {}
        log.debug(f"Loading project config: {file}")
        self._params.update(data if data is not None else {})

    @property
    def config(self):
        return self._params

    @property
    def solution(self) -> str:
        return self.find('project.solution')

    @property
    def allowed(self) -> List[str]:
        return self.find('submission.allowed', ['*'])

    @property
    def subdir(self) -> str:
        return self.find('submission.subdir')

    @property
    def namespace(self) -> str:
        return self.find('project.namespace', self._project.repo_namespace)

    @property
    def url(self) -> str:
        return self.find('project.url', self._project.repo_url_https)

    @property
    def git_url(self) -> str:
        value = self.find('project.repo_url')
        if value is None:
            self.set('project.repo_url', self._project.repo_url_ssh)
        return self.find('project.repo_url')


class Submission(utils.DirectoryWrapper):
    def __init__(self, base_path: Union[str, Path], name: str):
        super().__init__(base_path, name)
        self._sub_results = SubmissionResults(self)
        self._sub_sources = SubmissionSources(self)
        self._sub_workspaces = SubmissionWorkspaces(self)
        self._config = SubmissionConfig(self)
        self._project_config = None
        self._config.load()

    @property
    def config(self) -> 'SubmissionConfig':
        return self._config

    @property
    def project_config(self) -> 'ProjectConfig':
        if self._project_config is None:
            self._project_config = ProjectConfig(self.sources, {})
        return self._project_config

    @property
    def results(self) -> 'SubmissionResults':
        return self._sub_results

    @property
    def sources(self) -> 'SubmissionSources':
        return self._sub_sources

    @property
    def workspaces(self) -> 'SubmissionWorkspaces':
        return self._sub_workspaces

    def info(self) -> Dict:
        return {
            'name': self.name,
            'path': self.str_path,
            'solution_path': str(self.sources.solution_path),
            'git': self.sources.git_info(),
            'config': self.config.as_dict() if self.config.config_file.exists() else None
        }


class SubmissionResults(utils.DirectoryWrapper):
    def __init__(self, submission: 'Submission'):
        super().__init__(submission.path, name='res')
        self._submission = submission

    @property
    def sub(self) -> 'Submission':
        return self._submission


class SubmissionSources(utils.GitResource):

    def __init__(self, submission: 'Submission'):
        super().__init__(submission.path, name='src')
        self._submission = submission
        self._sol_path = None

    @property
    def sub(self) -> 'Submission':
        return self._submission

    @property
    def solution_path(self) -> Path:
        if self._sol_path is None:
            self._sol_path = self.path / self.sub.config.solution_files
        return self._sol_path


class SubmissionWorkspaces(utils.DirectoryWrapper):
    def __init__(self, submission: 'Submission'):
        super().__init__(submission.path, name='wrk')
        self._submission = submission

    @property
    def sub(self) -> 'Submission':
        return self._submission

    def create(self, suffix: str) -> utils.GitResource:
        workspace_name = f"{self.name}-{suffix}"
        log.info(f"[WORKSPACE] Create: {workspace_name}")
        return utils.GitResource(self.path, workspace_name)


class SubmissionConfig(utils.MappingBase):
    CFG_FILE = 'kontr-submission.yaml'

    def __init__(self, submission: 'Submission'):
        super().__init__(params={})
        self._submission = submission
        self._loaded = None

    @property
    def config_file(self) -> Path:
        return self._submission.sources.solution_path / self.__class__.CFG_FILE

    def init(self, params):
        self._params = params

    def load(self):
        if self.config_file.exists():
            log.debug(f"Loading submission config: {self.config_file}")
            self._params = self._submission.yaml(self.config_file)

    def dump(self, params):
        data = self.as_dict()
        data.update(params)
        self.config_file.write_text(yaml.dump(data), encoding='utf-8')

    @property
    def config(self):
        return self._params

    @property
    def project_name(self) -> str:
        return self.find('project.name')

    @property
    def project_url(self) -> str:
        return self.find('project.url')

    @property
    def project_ref(self) -> str:
        return self.find('project.ref')

    @property
    def project_branch(self) -> str:
        return self.find('project.branch')

    @property
    def id(self) -> str:
        return self.find('id')

    @property
    def uuid(self) -> str:
        return self.find('uuid')

    @property
    def author(self) -> str:
        return self.find('author')

    @property
    def source_type(self) -> str:
        return self.find('source.type', default='git')

    @property
    def source_url(self) -> str:
        return self.find('source.url')

    @property
    def source_ref(self) -> str:
        return self.find('source.ref')

    @property
    def source_subdir(self) -> str:
        return self.find('source.subdir')

    @property
    def source_branch(self) -> str:
        return self.find('source.branch', 'master')

    @property
    def submit_params(self) -> str:
        return self.find('submit_params')

    @property
    def solution_files(self) -> str:
        return self.find('submission.solution', default='solution')
