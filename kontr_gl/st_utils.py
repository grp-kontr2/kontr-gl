import json
import re
import string
import logging
import os
import os.path
import shutil
import zipfile
import secrets
import hashlib
import checksumdir
from pathlib import Path
from typing import Union, Any, IO, List, Optional

import yaml

log = logging.getLogger(__name__)

AnyPath = Union[str, Path]

BLOCKSIZE = 65536
NOT_ALLOWED_FOR_NAME = string.punctuation.replace('_', '')


def is_forward_path(root: AnyPath, path: AnyPath):
    root = Path(root)
    path = Path(path)
    abs_path = os.path.abspath(root / path)
    abs_path = Path(abs_path)
    try:
        relative = abs_path.relative_to(root)
        relative_str = str(relative)
        return not relative_str.startswith("../")
    except ValueError:
        return False


def zipdir(zip_file_path: AnyPath, src_dir: AnyPath):
    """Zips the directory to the zip file
    Args:
        zip_file_path(Path,str): Zip file path
        src_dir(Path,str): Source directory
    """
    src_dir = str(src_dir)
    log.info(f"[ZIP] Creating archive: {zip_file_path} from {src_dir}")
    with zipfile.ZipFile(zip_file_path, "w",
                         compression=zipfile.ZIP_DEFLATED) as zf:
        base_path = os.path.normpath(src_dir)
        for dirpath, dirnames, filenames in os.walk(src_dir):
            for name in sorted(dirnames):
                path = os.path.normpath(os.path.join(dirpath, name))
                zf.write(path, os.path.relpath(path, base_path))
            for name in filenames:
                path = os.path.normpath(os.path.join(dirpath, name))
                if os.path.isfile(path):
                    zf.write(path, os.path.relpath(path, base_path))


def unzipdir(archive_file: AnyPath, dest: AnyPath) -> Path:
    """Unzip the directory
    Args:
        archive_file(str, Path):
        dest(Path):

    Returns(Path): to to outout directory
    """
    log.info(f"[ZIP] Extracting archive {archive_file} to {dest}")
    shutil.unpack_archive(archive_file, str(dest))
    return dest


def delete_dir(directory: Path):
    """Deletes directory

    Args:
        directory(Path): Path to the dir
    """
    directory = str(directory)
    log.debug(f"[DEL] Deleting dir: {directory}")
    shutil.rmtree(directory)


def create_dir(directory: Path):
    """Creates a directory

    Args:
        directory: Path to the dir
    """
    if not directory.exists():
        log.debug(f"[MKD] Create dir: {directory}")
        directory.mkdir(parents=True)


def copy_and_overwrite(src: Path, dst: Path, ignore_errors=True):
    """Copies and overrides the files in the directory

    Args:
        src(Path): Source dir
        dst(Path): Destination dir
    """
    if dst.exists():
        shutil.rmtree(str(dst), ignore_errors=ignore_errors)
    shutil.copytree(str(src), str(dst))


def clone_file(file: Path, dst: Path, src: Path):
    """Clones the file (full path), to the destination dir, src dir is for
    a relative path

    Args:
        file(Path): File path
        dst(Path): Destination path
        src(Path): Base path to which file path is relative to
    """
    rel_path = file.relative_to(src)
    if str(rel_path).startswith('.git'):
        return
    final_path = dst / rel_path
    create_dir(final_path.parent)
    if file.is_dir():
        log.debug(f"[CPD] Copy dir: {file} -> {final_path}")
        copy_and_overwrite(src=file, dst=final_path)
    else:
        log.debug(f"[CPD] Copy file: {file} -> {final_path}")
        shutil.copy2(src=str(file), dst=str(final_path))


def clone_files(src: AnyPath, dst: AnyPath, pattern: str):
    """Clone the files
    Args:
        src(Path): Source directory
        dst(Path): Destination directory
        pattern(str): Patter to be globed

    """
    log.debug(f"[CLN] Cloning files [{pattern}] : {src} -> {dst}")
    src = Path(src)
    dst = Path(dst)
    files = src.rglob(pattern=pattern)
    for fpath in files:
        clone_file(fpath, dst=dst, src=src)


def dict_dig(dictionary: dict, path: list):
    for item in path:
        if item not in dictionary:
            return None
        dictionary = dictionary[item]
        if not isinstance(dictionary, dict):
            return dictionary
    return dictionary


def get_directory_structure(basedir: AnyPath) -> dict:
    """Creates a nested dictionary that represents the folder structure of rootdir

    Args:
        basedir(str, path): Base directory from which to start walking

    Returns(dict): Dictionary representing the file structure
    """
    dir_content = {}
    basedir = str(basedir)
    basedir = basedir.rstrip(os.sep)

    current = None

    for root, dirs, files in os.walk(basedir):
        rel_path = os.path.relpath(root, basedir)
        rel_path = os.path.normpath(rel_path)
        if root == basedir:
            current = dir_content
        else:
            split_path = rel_path.split(os.sep)
            current = dict_dig(dir_content, split_path)

        for f_name in files:
            current[f_name] = os.path.join(rel_path, f_name)

        for d_name in dirs:
            current[d_name] = {}

    return dir_content


def compress_entity(entity) -> Path:
    """Compressing the entity to the zip file
    Arguments:
        entity(Entity) Entity instance
    Returns(Path): Path to the zip file
    """
    log.info(f"[ZIP] Compressing {entity.dir_name} to file: {entity.zip_path}")
    zipdir(entity.zip_path, entity.path)
    return entity.zip_path


def decompress_entity(entity) -> Path:
    log.info(f"[ZIP] Decompressing {entity.dir_name} to dir: {entity.path}")
    unzipdir(entity.zip_path, entity.path)
    return entity.path


def update_dir(src: Path, dst: Path):
    """Updates directory content

    Args:
        src(Path): Source dir
        dst(Path): Destination dir
    """
    log.debug(f"[UPD] Dir update: {src} -> {dst}")
    for root, dirs, files in os.walk(str(src)):
        for file in files:
            src_path = Path(os.path.join(root, file))
            clone_file(file=src_path, src=src, dst=dst)


def calculate_hash(path: Path) -> str:
    if path.is_dir():
        return checksumdir.dirhash(path, hashfunc='sha256')

    hasher = hashlib.sha256()

    with path.open("rb") as fd:
        buf = fd.read(BLOCKSIZE)
        while buf:
            hasher.update(buf)
            buf = fd.read(BLOCKSIZE)

    return hasher.hexdigest()


def get_size(path: Path) -> int:
    if path.is_file():
        return os.path.getsize(str(path))

    return sum(f.stat().st_size for f in path.glob('**/*') if f.is_file())


def remove_punctuation(s: str):
    table = s.maketrans('', '', NOT_ALLOWED_FOR_NAME)
    return s.translate(table)


def substitute_spaces(s, sub='_'):
    return sub.join(s.split())


def convert_to_snake(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def normalize_name(name):
    if name is None:
        return name
    name = convert_to_snake(name)
    name = name.lower()
    name = remove_punctuation(name)
    name = substitute_spaces(name)
    return name


def unique_name(name):
    return f"{name}_{secrets.token_urlsafe(8)}"


def load_yaml(fp) -> Any:
    with Path(fp).open("r") as stream:
        return yaml.load(stream, Loader=CustomYamlLoader)


class CustomYamlLoader(yaml.SafeLoader):
    """YAML Loader with `!include` constructor."""

    def __init__(self, stream: IO) -> None:
        """Initialise Loader."""

        try:
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir

        super().__init__(stream)


def construct_include(loader: CustomYamlLoader, node: yaml.Node) -> Any:
    """Include file referenced at node."""

    filename = os.path.abspath(os.path.join(loader._root, loader.construct_scalar(node)))
    extension = os.path.splitext(filename)[1].lstrip('.')

    with open(filename, 'r') as f:
        if extension in ('yaml', 'yml'):
            return yaml.load(f, loader)
        elif extension in ('json',):
            return json.load(f)
        else:
            return ''.join(f.readlines())


def find_file(path: Path, names: List['str'], exts: List['str']) -> Optional[Path]:
    path = Path(path)
    for name in names:
        for ext in exts:
            fp = path / f"{name}.{ext}"
            if fp.exists():
                return fp
    return None


def filter_files(from_path: Path, to_path: Path, allowed: Union[str, List] = "*"):
    filters = allowed.splitlines() if isinstance(allowed, str) else allowed
    for pattern in filters:
        log.info(f"[FLT] Filtering using filter for {pattern}")
        clone_files(src=from_path, dst=to_path, pattern=pattern)
    return to_path
