import logging
from pathlib import Path
from typing import Mapping, Any, MutableMapping

from dynaconf import settings
from dynaconf.base import Settings

from kontr_gl import entities, submit, gl_client

log = logging.getLogger(__name__)


class KontrGlApplication:
    __slots__ = ('_params', '_cfg', '_projects', '_submissions', '_storage_path', '_gitlab')

    def __init__(self, *params, **kwargs):
        pp = {}
        for item in params:
            pp.update(item)
        pp.update(kwargs)
        self._params = pp
        self._cfg = settings.from_env(self.env)
        self._projects = entities.KontrProjects(self)
        self._submissions = entities.KontrSubmissions(self)
        self._storage_path = None
        self._gitlab = gl_client.KontrGitlabService(self)

    @property
    def gitlab(self) -> gl_client.KontrGitlabService:
        return self._gitlab

    @property
    def storage_path(self) -> Path:
        if self._storage_path is None:
            self._storage_path = Path(self.cfg.get('storage.path')).resolve()
        return self._storage_path

    @property
    def env(self) -> str:
        return self._params.get('env', 'development')

    @property
    def cfg(self) -> Settings:
        return self._cfg

    @property
    def params(self) -> Mapping[str, Any]:
        return self._params

    @property
    def projects(self) -> 'entities.KontrProjects':
        return self._projects

    @property
    def submissions(self) -> 'entities.KontrSubmissions':
        return self._submissions

    def submit(self, params: MutableMapping[str, Any]):
        sub = self.submissions.create(params)
        project = self.projects.get(sub.config.project_name)

        if project is not None:
            project = self.projects.create(
                sub.config.project_name,
                url=sub.config.project_url,
                ref=sub.config.project_ref,
                branch=sub.config.project_branch
            )
        submitter = submit.Submitter(self, project, params)
        return submitter.submit()
