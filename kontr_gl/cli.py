import json
import logging
import sys
from pathlib import Path

import click
import yaml
from dynaconf import settings

from kontr_gl import kontr_app, __version__, log_config

log = logging.getLogger(__name__)

base_dir = Path(__file__).parent.parent

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

pass_app = click.make_pass_decorator(kontr_app.KontrGlApplication)


@click.group(help=f'Kontr Gitlab Executor CLI tool', context_settings=CONTEXT_SETTINGS)
@click.version_option(version=__version__)
@click.option('-L', '--log-level', help=f'Sets the log level (d|i|w|e) - default=w', default=None)
@click.option('-E', '--env', help=f'Sets the environment - default=None', default=None)
@click.pass_context
def main_cli(ctx=None, log_level=None, env=None, **kwargs):
    env = env if env is not None else settings.get('env', 'development')
    log_config.load(log_level)
    config = {**kwargs, 'env': env}
    kapp = kontr_app.KontrGlApplication(config)
    ctx.obj = kapp


@main_cli.command('config', help="Show a current configuration")
@click.option('-F', '--full', help="Shows whole configuration", is_flag=True, default=False)
@click.option('-f', '--format', help="Sets the output format", default='yaml')
@pass_app
def cli_config(app: kontr_app.KontrGlApplication, full=False, format='yaml'):
    result = app.cfg.as_dict() if full else settings.as_dict()
    print("Full configuration: " if full else f"Configuration for the environment '{app.env}':", file=sys.stderr)
    if format == 'json':
        print(json.dumps(result, indent=2))
    else:
        print(yaml.dump(result))


@main_cli.command('projects', help='List all registered homework projects')
@pass_app
def cli_projects(app: kontr_app.KontrGlApplication):
    if len(app.projects) == 0:
        print("No projects found.")
        return

    print("Projects: ", file=sys.stderr)
    for project in app.projects:
        print(f"- {project.name} - {project.path}")


@main_cli.group('project', help='Manage the project')
def cli_project():
    pass


@cli_project.command('new', help='Create a new project')
@click.option('-u', '--url', help="Git url of the project - for clone, push or pull")
@click.option('-r', '--ref', help="Git reference to checkout")
@click.argument('name')
@pass_app
def cli_project_new(app: kontr_app.KontrGlApplication, url: str = None, name: str = None, ref=None):
    app.projects.create(name, url, ref)


@cli_project.command('get', help='Get a project information')
@click.argument('name')
@pass_app
def cli_project_get(app: kontr_app.KontrGlApplication, name: str = None):
    project = app.projects.get(name)
    print(yaml.dump(project.info()))


@cli_project.command('rm', help='Remove a project information')
@click.argument('name')
@pass_app
def cli_project_rm(app: kontr_app.KontrGlApplication, name: str = None):
    app.projects.delete(name)


@cli_project.command('update', help='Update a project (pull the new version)')
@click.argument('name')
@pass_app
def cli_project_rm(app: kontr_app.KontrGlApplication, name: str = None):
    app.projects.update(name)


@main_cli.command('submit', help='Create a new submission using the command line parameters')
@click.option('-P', '--project', help='Sets the project for which the submission should be created')
@click.option('-U', '--project-url', help='Sets the project for which the submission should be created')
@click.option('-p', '--param', nargs=-1, help='Pass the additional parameters')
@pass_app
def cli_submit(app: kontr_app.KontrGlApplication, project: str, param: List[str]):
    pass


@main_cli.command('submit-json', help='Create a new submission using the json file configuration')
@click.option('-f', '--file', help='JSON file with an instructions (if not provided - stdout)', default=None)
@pass_app
def cli_submit_json(app: kontr_app.KontrGlApplication, file: str = None):
    if file is None:
        params = json.load(fp=sys.stdin)
    else:
        with open(file, 'r') as stream:
            params = json.load(stream)
    app.submissions.create(params)


@main_cli.command('submit-yaml', help='Create a new submission using the yaml file configuration')
@click.option('-f', '--file', help='YAML file with an instructions (if not provided - stdout)', default=None)
@pass_app
def cli_submit_yaml(app: kontr_app.KontrGlApplication, file: str = None):
    pass


@main_cli.command('submissions', help="List all currently processed submissions")
@pass_app
def cli_submissions(app: kontr_app.KontrGlApplication):
    if len(app.submissions) == 0:
        print("No submissions found.")
        return

    print("Submissions: ", file=sys.stderr)
    for submission in app.submissions:
        print(f"- {submission.name} - {submission.path}")


@main_cli.group('submission', help='Manage the submission')
def cli_submission():
    pass


@cli_submission.command('get', help='Get information about the submission')
@click.argument('sid')
@pass_app
def cli_submission_get(app: kontr_app.KontrGlApplication, sid: str):
    submission = app.submissions.get(sid)
    print(yaml.dump(submission.info()))


@cli_submission.command('rm', help='Delete a submission')
@click.argument('sid')
@pass_app
def cli_submission_rm(app: kontr_app.KontrGlApplication, sid: str):
    submission = app.submissions.delete(sid)



if __name__ == '__main__':
    main_cli()
