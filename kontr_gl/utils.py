import collections.abc
import functools
import json
import logging
import os
import shutil
from pathlib import Path
from typing import Any, Iterator, MutableMapping, Union, Optional, Dict, Mapping, List

import git
import giturlparse
import yaml

from kontr_gl import st_utils, errors

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from kontr_gl import KontrGlApplication

log = logging.getLogger(__name__)


class MappingBase(collections.abc.MutableMapping):
    def __init__(self, params=None):
        self._params = {**(params or {})}

    @property
    def params(self) -> Dict[str, Any]:
        return self._params

    def __setitem__(self, k: str, v: Any):
        self.params[k] = v

    def __delitem__(self, key: str) -> None:
        del self.params[key]

    def __getitem__(self, key: str) -> Any:
        return self.params.get(key)

    def __len__(self) -> int:
        return len(self._params)

    def __iter__(self) -> Iterator:
        return iter(self.params)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}: {self.params}"

    def as_dict(self) -> Dict[str, Any]:
        return self.params

    def as_json(self) -> str:
        return json.dumps(self.params)

    def as_yaml(self) -> str:
        return yaml.dump(self.params)

    def __str__(self) -> str:
        return str(self.as_dict())

    def get(self, key, default=None, use_find=False, sep='.') -> Optional[Any]:
        if use_find:
            return self.find(key, default=default, sep=sep, raises=False)
        return self.params.get(key, default)

    def find(self, path: str, default=None, sep='.', raises=False):
        return dict_find(self, path=path, default=default, sep=sep, raises=raises)

    def find_set_default(self, path: str, default=None, sep='.'):
        value = self.find(path=path, default=default, sep='.')
        if value is None:
            value = default
            self.set(path=path, value=default, sep=sep)
        return value

    def set(self, path: str, value: Any, sep='.'):
        return dict_set(self, path=path, value=value, sep=sep)

    def merge(self, snd: dict, path=None, override=True, list_extend=False):
        return dict_merge(
            self,
            snd,
            path=path,
            override=override,
            list_extend=list_extend
        )


class DirectoryWrapper:
    @classmethod
    def from_dir(cls, path: Union[str, Path]):
        path = Path(path).resolve()
        return cls(base_path=path.parent, name=path.name)

    def __init__(self, base_path: Union[str, Path], name: str):
        self._name = name
        self._base_dir = Path(base_path).resolve()
        self._path = self.base_dir / self._name

    @property
    def name(self) -> str:
        return self._name

    @property
    def base_dir(self) -> Path:
        return self._base_dir

    @property
    def path(self):
        return self._path

    @property
    def str_path(self) -> str:
        return str(self.path)

    @property
    def zip_path(self) -> Path:
        return self.base_dir / f"{self.name}.zip"

    def delete(self):
        if self.path.exists():
            log.info(f"[DEL] Deleting directory {self.path} for {self.name}")
            shutil.rmtree(str(self.path), ignore_errors=True)
        if self.zip_path.exists():
            log.info(f"[DEL] Deleting zip file {self.zip_path} for {self.name}")
            self.zip_path.unlink()

    def zip_files(self) -> Optional[Path]:
        if not self.path.exists():
            log.warning(f"[ZIP] File path not exists - {self.path} for {self.name}")
            return None
        log.info(f"[ZIP] Zipping {self.path} -> {self.zip_path} for {self.name}")
        st_utils.zipdir(self.zip_path, self.path)
        return self.zip_path

    def unzip_files(self):
        if not self.zip_path.exists():
            log.warning(f"[ZIP] File path not exists - {self.zip_path} for {self.name}")
            return None
        log.info(f"[ZIP] Unzipping {self.zip_path} -> {self.path} for {self.name}")
        st_utils.unzipdir(self.zip_path, self.path)

    def exists(self, path: Union[str, Path] = None) -> bool:
        return self.resolve(path).exists()

    def safe_get(self, path_query: str, as_wrapper=False):
        path_query = Path(path_query)
        if not st_utils.is_forward_path(self.path, path_query):
            log.error(f"Not a valid path: {path_query} for: {self.path}/{path_query}")
            raise errors.InvalidPath(f"Not a valid path: {path_query} "
                                     f"for: {self.path}/{path_query}")
        return self.get(path_query, as_wrapper=as_wrapper, throws=True)

    def resolve(self, path_query: Union[str, Path] = None) -> Path:
        if path_query is None:
            return self.path

        pth = Path(path_query)
        if not pth.is_absolute():
            pth = self.path / pth

        return pth.resolve()

    def get(self, path_query: str, as_wrapper=False, throws=False) -> \
            Union[None, 'DirectoryWrapper', Path]:
        """Gets file based on relative path query
        Examples:
            path_query = "src/main.c"
            root = "/storage/submission"
            Result: "/storage/submission/src/main.c"
        Args:
            path_query(str): Path query
            as_wrapper(bool): Whether it be returned as wrapper
            throws(bool): Whether it should throw or return None
        Returns(Path): Absolute path to requested file
        """
        path = self.resolve(path_query)
        if not path.exists():
            log.error(f"[STORAGE] Path {path} not exists!")
            if throws:
                raise errors.NotFoundError(f"Path {path} not exists!")
            else:
                return None
        if as_wrapper:
            return DirectoryWrapper(base_path=path.parent, name=path.name)
        return path

    def text(self, path='', encoding='utf-8', throws=True) -> Optional[str]:
        fp = self.get(path, throws=throws)
        if fp is None:
            return None
        return fp.read_text(encoding=encoding)

    def bytes(self, path='', throws=False) -> Optional[bytes]:
        fp = self.get(path, throws=throws)
        if fp is None:
            return None
        return fp.read_bytes()

    def yaml(self, path='', throws=False) -> Optional[Any]:
        fp = self.get(path, throws=throws)
        if fp is None:
            return None

        return st_utils.load_yaml(fp)

    def json(self, path='', throws=True) -> Optional[Any]:
        fp = self.get(path, throws=throws)
        if fp is None:
            return None

        with fp.open('r') as fd:
            return json.load(fd)

    def create_dir(self, check=False) -> Path:
        if self.path.exists():
            if check:
                raise errors.DirectoryAlreadyExists(f"Directory already exists: \"{self.path}\"")
        else:
            log.info(f"[STORAGE] Create dir: {self.path}")
            self.path.mkdir(parents=True)
        return self.path

    def mkdir(self, check=False) -> Path:
        return self.create_dir(check=check)

    def tree(self) -> dict:
        """Gets a tree representation of file structure of the submission
        Returns(dict):
        """
        return st_utils.get_directory_structure(self.path)

    def clean(self):
        self.zip_files()
        if self.path.exists():
            log.info(f"[DEL] Deleting directory {self.path} for {self.name}")
            shutil.rmtree(str(self.path), ignore_errors=True)

    def copy_tree(self, src_path: Path, subpath=None):
        full_path = self.path / subpath if subpath else self.path
        if not full_path.parent.exists():
            full_path.parent.mkdir(parents=True)
        st_utils.copy_and_overwrite(src=src_path, dst=full_path)

    def write_text(self, path: str, content: str, encoding='utf-8'):
        fp = self.path / path
        fp.write_text(content, encoding=encoding)

    def write_bytes(self, path: str, content: bytes):
        fp = self.path / path
        fp.write_bytes(content)


def _is_local_path(url: str) -> bool:
    try:
        Path(url).resolve()
    except OSError as ex:
        return False
    return True


class GitResource(DirectoryWrapper):
    @property
    def repo(self) -> git.Repo:
        return git.Repo(self.str_path)

    @property
    def _git_url_parse(self) -> Optional[giturlparse.GitUrlParsed]:
        remote = self.repo.remote('origin')
        if remote is None:
            return None

        url = list(remote.urls)[0]
        if not _is_local_path(url):
            return giturlparse.parse(url)
        return None

    @property
    def repo_url_ssh(self) -> Optional[str]:
        if self._git_url_parse is None:
            return None
        return self._git_url_parse.url2ssh

    @property
    def repo_namespace(self) -> Optional[str]:
        if self._git_url_parse is None:
            return None
        return f"{self.repo_owner}/{self.repo_name}"

    @property
    def repo_url_https(self) -> Optional[str]:
        if self._git_url_parse is None:
            return None
        return self._git_url_parse.url2https

    @property
    def repo_owner(self) -> Optional[str]:
        if self._git_url_parse is None:
            return None
        return self._git_url_parse.owner

    @property
    def repo_name(self) -> Optional[str]:
        if self._git_url_parse is None:
            return None
        return self._git_url_parse.repo

    def clone(self, url: str, ref: str = None, branch: str = None) -> git.Repo:
        if not self.path.exists():
            self.path.mkdir(parents=True)

        repo = git.Repo.clone_from(url=url, to_path=self.str_path)
        self.checkout(ref=ref, branch=branch)
        return repo

    def checkout(self, ref: str = None, branch: str = None, new=False) -> git.Repo:
        checkout = None
        if ref is not None:
            checkout = ref
        elif branch is not None:
            checkout = f'refs/heads/{branch}'

        if checkout is not None:
            args = [checkout] if not new else ['-b', checkout]
            self.repo.git.checkout(args)
        return self.repo

    def commit(self, message: str, adds: List = None):
        if adds:
            self.repo.index.add(adds)
        self.repo.index.commit(message)

    def pull(self, remote='origin', branch='master') -> git.Repo:
        self.repo.remotes[remote].pull(branch)
        return self.repo

    def push(self, remote='origin', branch='master') -> git.Repo:
        self.repo.remotes[remote].push(branch)
        return self.repo

    def info(self) -> Dict:
        return {
            'name': self.name,
            'path': self.str_path,
            'git': self.git_info(),
        }

    def git_info(self) -> MutableMapping[str, Any]:
        return {
            'hash': self.repo.head.commit.hexsha,
            'size': st_utils.get_size(self.path),
            'head_name': self.repo.head.name,
            'head_ref': self.repo.head.ref,
            'head_commit': {
                'message': self.repo.head.commit.message,
                'author': self.repo.head.commit.author,
                'commited_date': self.repo.head.commit.committed_date,
                'authored_date': self.repo.head.commit.authored_date,
                'digest': self.repo.head.commit.hexsha
            },
            'repo': {
                'ssh': self.repo_url_ssh,
                'https': self.repo_url_https,
                'owner': self.repo_owner,
                'name': self.repo_name,
                'namespace': self.repo_namespace,
            }
        }


class DirectoryMapping(collections.abc.MappingView):
    def __init__(self, app: 'KontrGlApplication'):
        self._app = app

    @property
    @functools.lru_cache()
    def path(self) -> Path:
        return Path(self._app.cfg.get('storage.path'))

    @property
    def str_path(self) -> str:
        return str(self.path)

    def __delitem__(self, name: str) -> None:
        self.delete(name)

    def __getitem__(self, name: str) -> 'ResourceWrapper':
        return GitResource(self.path, name)

    def __len__(self) -> int:
        return len(os.listdir(self.str_path)) if self.path.exists() else 0

    def __iter__(self) -> Iterator['ResourceWrapper']:
        if not self.path.exists():
            return iter([])
        return iter(self.get(item) for item in os.listdir(str(self.path)))

    def delete(self, name: str):
        self.get(name).delete()

    def get(self, name: str) -> 'ResourceWrapper':
        return self[name]


def dict_find(mapping: Mapping, path: str, default=None, sep='.', raises=False) -> Any:
    current = mapping
    if path is None or path == '':
        return mapping
    parts = path.split(sep)
    for part in parts:
        if part.isdigit() and isinstance(current, list):
            index = int(part)
            if index < len(current):
                current = current[index]
            elif raises:
                raise IndexError(f"Index {index} of out range")
            else:
                return default
        elif current is None:
            if raises:
                raise IndexError(f"Unable to access key {part} on None")
            else:
                return default
        else:
            current = current.get(part)

    return current


def dict_set(mapping: MutableMapping, path: str, value: Any, sep='.'):
    if path is None or path == '':
        return
    parts = path.split(sep)
    current = mapping
    prev_part = None
    prev = None
    for part in parts:
        if part not in current:
            current.setdefault(part, {})
        prev_part = part
        prev = current
        current = current[part]
    if prev is not None:
        prev[prev_part] = value


def dict_merge(fst, snd, path=None, override=True, list_extend=False):
    "merges snd into fst"
    if path is None: path = []
    for key in snd:
        if key in fst:
            if isinstance(fst[key], dict) and isinstance(snd[key], dict):
                dict_merge(fst[key], snd[key], path + [str(key)], override=override)
            if isinstance(fst[key], list) and isinstance(snd[key], list):
                fst[key].extend(snd[key])
            elif fst[key] == snd[key]:
                pass  # same leaf value
            elif override:
                fst[key] = snd[key]
        else:
            fst[key] = snd[key]
    return fst
