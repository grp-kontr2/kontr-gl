# Kontr Gitlab CI Execution Tool 


[![pipeline status](https://gitlab.fi.muni.cz/grp-kontr2/kontr-gl/badges/master/pipeline.svg)](https://gitlab.fi.muni.cz/grp-kontr2/kontr-gl/commits/master)


Tool to submit the homework using and execute it with the Gitlab CI.

Responsibilities:

- Pull the current project (base) version with tests
- Pull the students submission
- Filter out unnecessary files from the student submission
- Copy student files to the specified folder in the project folder (example: ``sources``)
- Create a new branch based on the submission and push the new branch to the Gitlab
- Create a new merge request on project repository
- Tag a submission on the student's repository
- Waits until the pipeline finishes and downloads jobs' artifacts.


## Install

Requirements:

- Python 3.6 or greater
- [Poetry](https://poetry.eustace.io/docs/) 


```bash
git clone https://gitlab.fi.muni.cz/grp-kontr2/kontr-gl.git
cd kontr-gl
poetry install
```


## Command line interface

```bash
$ poetry run python -m "kontr_gl.cli" --help
Usage: cli.py [OPTIONS] COMMAND [ARGS]...

  Kontr Gitlab Executor CLI tool

Options:
  --version             Show the version and exit.
  -L, --log-level TEXT  Sets the log level (d|i|w|e) - default=w
  -E, --env TEXT        Sets the environment - default=None
  -h, --help            Show this message and exit.

Commands:
  config       Show a current configuration
  project      Manage the project
  projects     List all registered homework projects
  submission   Manage the submission
  submissions  List all currently processed submissions
  submit       Create a new submission using the command line parameters
  submit-json  Create a new submission using the json file configuration
  submit-yaml  Create a new submission using the yaml file configuration
```


